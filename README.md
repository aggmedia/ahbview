AHBView is a Swift subclass of WKWebView in iOS, which allows for opening and displaying macOS Apple Help Books.

We recognise that iOS applications should not contain help pages or views which use long text descriptions to explain how the application works. iOS apps should be intuitive and obvious, and their user interface is all a user should require.

However there are some edge cases where iOS apps may require some integrated documentation, either as part of a larger description of data or context, or due to porting or conversion from another operating system such as macOS. An example might be a macOS D&D style game which contains a lot of information on character and game play mechanics, or a statistical analysis package which includes a lot of information on the formulae and statistical methods used. In moving these apps to iOS, it might be easier to initially provide this as an embedded version of the macOS Help Book already used in the macOS app, as a stepping stone to a later full integration in an iOS interface.

AHBView provides developers a way to display an Apple Help Book in an iOS view, and includes support for dynamic font-size calculation, CSS block selectors by device and orientation, and localisation.

---

## Adding AHBView to your Xcode project

1. Add the AHBView class file to your Xcode project. This is the only file you'll need from this repository.
2. Make sure Swift integration is enabled and configured in your Xcode project, and that the project now builds correctly.

AHBView behaves identically to WKWebView, until you call the loadHelpBook() func to load a Help Book.

## Using AHBView in your project

Using AHBView is easy.

1. Create a WKWebView in your project, either in Interface Builder or manually via code. If you already have a WKWebView which you'd like to use then use that instead.
2. Configure the WKWebView in Interface Builder or manually via the WKWebView class or WKConfiguration class. See Apple's documentation for WKWebView for more details.
3. Test that the WKWebView is working correctly and your project builds correctly.
4. Change the class of your WKWebView to AHBView.
5. Test again that your view is working correctly and your project builds correctly.
6. Add the target Apple Help Book to your project, if not already added.
7. Make sure the Apple Help Book is included in the target iOS build, by selecting it Xcode and selecting the appropriate "Target Membership" in the file inspector tab on the right sidebar.
8. Finally, add the following code to your project to load the Help Book into the view:

        let helpBookPath = Bundle.main.url(forResource: "<name of help book in bundle>", withExtension: "help")
        _ = ahbView.loadHelpBook(helpBookPath: helpBookPath!)

This code will load the help book and display it.

## How it works

When loadHelpBook() is called, the Help Book is opened directly from the specified URL and parsed.

The Help Book's Info.plist file is parsed to an NSDictionary and provided to the caller via:

    public var helpBookInfoPlist: NSDictionary? = nil
    
Next AHBView determines which Help Book localisation to use, by applying a number of checks.

1. If the current iOS locale (language and dialect) matches a localisation in the Help Book, then use that.
2. If the language code of the current iOS locale matches a localisation in the Help Book, then use that.
3. If the CFBundleDevelopmentRegion value in the Help Book Info.plist exists as a localisation in the Help Book, then use that.
4. Otherwise use the first localisation in the Help Book.

AHBView then determines the current iOS system state and adds those as CSS selectors to the WKConfiguration as a WKUserScript, so that each HTML document that is loaded has the new selectors.

The index.html file is then loaded into the view via the WKWebView superclass.

The localisation to use is recalculated every time loadHelpBook() is called, so changes in iOS locale can be reflected in AHBView by simply calling loadHelpBook() again.

## AHBView parameters

AHBView provides a few additional parameters for controlling display of Help Books.

The full method signature is:

    func loadHelpBook(helpBookPath: URL, dynamicBodyPointSize: CGFloat? = UIFont.preferredFont(forTextStyle: .body).pointSize, debug: Bool = false) -> Bool

If the Help Book loads correctly, then this method returns true, otherwise if there was an error it will return false.

dynamicBodyPointSize - AHBView can optionally add CSS selectors that specify the font size for various devices and orientations. You specific the base iOS font size, and the CSS selectors calculate the correct WKWebView font size for the device. The default if this parameter is not specified, is to use the UIFont.preferredFont(forTextStyle: .body) font size. You can also specify nil to turn this feature off. You can still hard code your own font-size by CSS in your Help Book, but this will add additional px definitions to the HTML body element which will override any font sizes you've specificed on the body element.

We recommend using the default here, and removing any hard coded font-size which is specified by px in your Help Book. You can then use font-size changes specified by em or % for specific elements to based on the dynamic size set by AHBView.

debug - If true, then AHBView places a copy of the last loaded HTML document into its helpBookLastHtmlSource property, so it can be referenced by the caller if needed. AHBView will also print the source to the log. This allows you to see what changes AHBView is making to the Help Book HTML, and to see what changes you might need to make to Help Book to improve its display. The default if not specified, is false, which turns these debugging features off.

You can also turn debugging on and off at any time by changing the following property:

    public var debug: Bool

## CSS selectors

AHBView adds the following additional boolean CSS block selectors to the HTML head element. If true, the selector is set to display: block, or if false is set to display: none. This allows you to optionally build a Help Book which may be used in both macOS and iOS in different orientations.

Use the debug parameter described above, to see the full changes that AHBView makes to the Help Book HTML source.

    .macos - This is always false.
    .ios - This is always true.
    .isLandscape
    .isPortrait
    .isPhone
    .isPad
    .isTv
    .isCarPlay

Note that because these are added by AHBView, they won't exist in the Help Book when it is opened by the Help Book Viewer in macOS. You'll need to think through this carefully if attempting to build a Help Book that works in both macOS and iOS with different block elements depending on the operating system.
