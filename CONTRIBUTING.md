We love that you're interested in contributing to this project!

To speed possible changes, please consider the following guidelines.

## Prefer Pull Requests

If you know exactly how to implement the feature being suggested or fix the bug
being reported, open a pull request instead of an issue. Pull requests are easier than
patches or inline code blocks for discussing and merging the changes.

If you can't make the change yourself, please open an issue after making sure
that one doesn't already exist.

## Contributing Code

Fork this repository with a name matching or similar to the topic or issue, make your
changes, and send us a pull request.

All code contributions should match Swift coding conventions.

Thanks for contributing!
