//
// AppleHelpBookView.swift
//
// Version 1.0.
//
// Created by Richard Bennett on 16/6/20.
//
// The MIT License
//
// Copyright (c) 2020 AHBView contributors
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

import UIKit
import WebKit

class AHBView: WKWebView, WKNavigationDelegate {

    // MARK: Public properties

    /// The URL path to the Help Book
    public var helpBookPath: URL? {
        return ahbHelpBookPath
    }

    /// The parsed Info.plist for the Help Bok
    public var helpBookInfoPlist: NSDictionary? {
        return infoPlist
    }

    /// The last loaded HTML source
    public var helpBookLastHtmlSource: String? {
        return lastHtmlSource
    }

    /// The localisation currently being used for this Help Book
    public var helpBookLocalisation: String {
        return localeIdentifier
    }

    /// The current debug setting
    public var debug: Bool = false

    // MARK: - Private properties

    /// The original point size specified by the caller
    private var dynamicBodyPointSize: CGFloat? = nil

    // Exposed via read only properties
    private var ahbHelpBookPath: URL? = nil
    private var infoPlist: NSDictionary? = nil
    private var lastHtmlSource: String? = ""
    private var localeIdentifier: String = ""

    private var helpBookResourcesPath: URL? {
        return ahbHelpBookPath?.appendingPathComponent("Contents", isDirectory: true).appendingPathComponent("Resources", isDirectory: true)
    }

    private var localisedHelpBookBasePath: URL? {
        return helpBookResourcesPath?.appendingPathComponent(localeIdentifier, isDirectory: true).appendingPathExtension("lproj")
    }

    private var localisedHelpBookIndexPath: URL? {
        return localisedHelpBookBasePath?.appendingPathComponent("index").appendingPathExtension("html")
    }

    private var infoPlistPath: URL? {
        return ahbHelpBookPath?.appendingPathComponent("Contents", isDirectory: true).appendingPathComponent("Info.plist")
    }

    // MARK: - Methods

    /**
     Loads a help book referenced by URL, and displays it in this WKWebView.

     - Parameters:
       - helpBookPath: The URL to the help book to load.
       - dynamicBodyPointSize: The iOS font point size upon which to base the help book CSS, or nil for none. Defaults to UIFont.preferredFont(forTextStyle: .body).pointSize
       - debug: If true copy every loaded page into var lastHtmlSource and displays it on the output log.
     - Returns: Whether the book loaded correctly (true) or not (false).
     */
    public func loadHelpBook(helpBookPath: URL, dynamicBodyPointSize: CGFloat? = UIFont.preferredFont(forTextStyle: .body).pointSize, debug: Bool = false) -> Bool {

        self.ahbHelpBookPath = helpBookPath
        self.debug = debug
        self.dynamicBodyPointSize = dynamicBodyPointSize

        infoPlist = NSDictionary(contentsOf: infoPlistPath!)

        // find our starting locale identifier
        repeat {

            // see if we have a localisation for the current locale
            localeIdentifier = NSLocale.current.identifier
            if FileManager.default.fileExists(atPath: localisedHelpBookBasePath!.path) {
                break
            }

            // what about just the language code?
            if NSLocale.current.languageCode != nil {
                localeIdentifier = NSLocale.current.languageCode!
                if FileManager.default.fileExists(atPath: localisedHelpBookBasePath!.path) {
                    break
                }
            }

            // try the bundle development region, although if that exists then so would the language code
            if infoPlist!["CFBundleDevelopmentRegion"] as? String != nil {
                localeIdentifier = infoPlist!["CFBundleDevelopmentRegion"] as! String
                if FileManager.default.fileExists(atPath: localisedHelpBookBasePath!.path) {
                    break
                }
            }

            // none exist, so just grab the first one
            do {
                localeIdentifier = ""
                let directoryList = try FileManager.default.contentsOfDirectory(atPath: helpBookResourcesPath!.path)
                directoryList.forEach( { directoryName in
                    if directoryName.hasSuffix(".lproj") && localeIdentifier == "" {
                        let index = directoryName.lastIndex(of: ".")!
                        localeIdentifier = String(directoryName[..<index])
                    }
                })
            } catch {
                // none. starting to think that this is not actually a help book
                localeIdentifier = ""
                return false
            }

            // all done

        } while false

        // get WKWebView navigation requests (only used for our debug pulling of source code)
        self.navigationDelegate = self

        // set all the dynamic CSS selectors
        let isLandscape = UIDevice.current.orientation.isLandscape ? "block" : "none"
        let isPortrait = !UIDevice.current.orientation.isLandscape ? "block" : "none"
        var isPhone = ""
        var isPad = ""
        var isTv = ""
        var isCarPlay = ""
        switch UIDevice.current.userInterfaceIdiom {
            case .phone:
                isPhone = "block"
                isPad = "none"
                isTv = "none"
                isCarPlay = "none"
            case .pad:
                isPhone = "none"
                isPad = "block"
                isTv = "none"
                isCarPlay = "none"
            case .tv:
                isPhone = "none"
                isPad = "none"
                isTv = "block"
                isCarPlay = "none"
            case .carPlay:
                isPhone = "none"
                isPad = "none"
                isTv = "none"
                isCarPlay = "block"
            default:
                isPhone = "none"
                isPad = "none"
                isTv = "none"
                isCarPlay = "none"
        }

        // convert body font size to WKWebView font size. WKWebView resizes html fonts to fit
        // the frame size, so we pre-empt that with our preferred size
        // (https://riptutorial.com/ios/example/24811/matching-dynamic-type-font-size-in-wkwebview)

        // on iPad, landscape text is larger than preferred font size
        var portraitMultiplier = CGFloat(0.8) // originally 1.0
        var landscapeMultiplier = CGFloat(0.4) // originally 0.5
        if UIDevice.current.model.range(of: "iPhone") != nil {
            portraitMultiplier = CGFloat(2.4) // originally 3.0
            landscapeMultiplier = CGFloat(1.2) // originally 1.5
        }
        let bodyFontSize = dynamicBodyPointSize != nil ? "@media all and (orientation:portrait) { body { font-size: calc(\(dynamicBodyPointSize! * portraitMultiplier)px + 1.0vw) } } @media all and (orientation:landscape) { body { font-size: calc(\(dynamicBodyPointSize! * landscapeMultiplier)px + 1.0vw) } }" : ""

        // build the javascript to add the head style element
        let addHeadStyleElement = """
var styleElement = document.createElement("style");
styleElement.textContent = "\(bodyFontSize) .macos { display: none } .ios { display: block } .isLandscape { display: \(isLandscape) } .isPortrait { display: \(isPortrait) } .isPhone { display: \(isPhone) } .isPad { display: \(isPad) } .isTv { display: \(isTv) } .isTV { display: \(isTv) } .isCarPlay { display: \(isCarPlay) } ";
document.documentElement.getElementsByTagName("head")[0].appendChild(styleElement);
"""

        // run it
        let script = WKUserScript(source: addHeadStyleElement, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
        configuration.userContentController.addUserScript(script)

        loadFileURL(localisedHelpBookIndexPath!, allowingReadAccessTo: helpBookPath)

        return true
    }

    /**
     Support for debug extraction and display of HTML source code.
     */
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {

        // pull in the loaded source for debugging purposes if required
        if debug {
            webView.evaluateJavaScript("document.getElementsByTagName('html')[0].innerHTML") { innerHTML, error in
                self.lastHtmlSource = innerHTML as? String
                print(self.lastHtmlSource!)
            }
        } else {
            lastHtmlSource = nil
        }
    }

}
